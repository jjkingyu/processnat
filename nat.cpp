/* Author: Jianjie Yu
 * Mail: jianjiy@clemson.edu
 * Date: 5/10/2018
*/
#include<iostream>
#include<unordered_map>
#include<string>
#include<vector>
#include<fstream>
#include<sstream>

/* Define a class to deal with the NAT process
 * and use a singleton as a basic design pattern
 * in this class, there are four member variable,
 * three hash map and an ouput used to write the result
 * to the file.
*/
class ProcessNat {
public:
	static ProcessNat& getInstance(); //get the singleton instance
	void natEntryHandler(std::string& str);  //handle the NAT entries, build the hashmap
	void readNatFile(std::string filename); //read the entries from NAT file
	void flowEntryHandler(std::string& s); //handle the flow entries, create the output file
	void readFlowFile(std::string filename, std::string outfile); //read the entries from flow file

private:
	//three hashmap for three rules
	std::unordered_map<std::string, std::string> rulePort, ruleIp, ruleBoth;
	std::ofstream output;
};
//get instance
ProcessNat& ProcessNat::getInstance() {
	static ProcessNat instance;
	return instance;
}

//This function used to split the string by given token
void split(std::string& s, 
		   char token, 
		   std::string& left, 
		   std::string& right) 
{
	std::istringstream ss( s );
	std::vector<std::string> res;
	while (!ss.eof())            
	{
		std::string x;            // here's a nice, empty string
		getline( ss, x, token );  // try to read the next field into it
		res.emplace_back(x);
	}
	left = res[0]; //get the split substr
	right = res[1];
}

//This function used to deal with each NAT entry and
//insert it to relative hashmap
void ProcessNat::natEntryHandler(std::string& str) {
	//split the entry to get the orignal and target
	//IP port pair
	std::string in, out;
	split(str, ',', in, out);
	
	// split ip port pair to get the ip address and port
	std::string ip, port;
	split(in, ':', ip, port);

	if ( ip == "*") {
		ruleIp.insert({port, out}); //if the rule don not consider IP, insert into relative hashmap
	}else if ( port == "*") {
		rulePort.insert({ip, out}); //if the rule don not consider Port, insert into relative hashmap
	}else {
		ruleBoth.insert({in, out}); //else insert orignal and target IP port pair to relative hashmap
	}
}

//Used to read the NAT file by each line
void ProcessNat::readNatFile(std::string filename) {
	std::string entry; //store the each single entry
	std::ifstream inputfile(filename); //define a input file stream

	if (inputfile.is_open()) {
		while (getline(inputfile, entry)) {
			if(entry.empty()) continue; //if the line is empty, skip it
			natEntryHandler(entry);  //deal with the entry
		}
		inputfile.close();
	}else {
		inputfile.close();
	}
}

//This function used to deal with each Flow entry and
//find whether there is a match
void ProcessNat::flowEntryHandler(std::string& s) {
	//define a iterator of hasmap
	std::unordered_map<std::string, std::string>::const_iterator it;

	//1. the rules 1 has high priority, first match this rule
	//if matched, write to output file
	if ((it = ruleBoth.find(s)) != ruleBoth.end()) {
		output << s <<"->"<< it->second << std::endl;
		return;
	}
	
	//2. the rules 2,3 has low priority, if match those two rules
	//write to output file
	std::string ip, port;
	split(s, ':', ip, port);

	if ((it = ruleIp.find(port)) != ruleIp.end()) {
		output << s <<"->"<< it->second << std::endl;
		return;
	}

	if ((it = rulePort.find(ip)) != rulePort.end()) {
		output << s <<"->"<< it->second << std::endl;
		return;
	}
	//if no match, write no match entry
	output << "No nat match for " << s << std::endl;
}

//Used to read the FLow file by each line, same as read NAT file
void ProcessNat::readFlowFile(std::string filename, std::string outfile) {
	std::string entry;
	std::ifstream inputfile(filename);
	output.open(outfile);

	if (inputfile.is_open()) {
		while (getline(inputfile, entry)) {
			if(entry.empty()) continue; //if the line is empty, skip it
			flowEntryHandler(entry); //deal with each entry
		}
		inputfile.close();
	}else{ 
		inputfile.close();
	}
}

int main(int argc, char* argv[]) {
	//define a three file
	std::string natFile, flowFile, outputFile;
	
	//get the three file from command
	natFile = argv[1];
	flowFile = argv[2];
	outputFile = argv[3];
	//define a reference to the ProcessNat class
	ProcessNat& helper = ProcessNat::getInstance();

	helper.readNatFile(natFile);
	helper.readFlowFile(flowFile, outputFile);
	
	return 0;
}
