## Description

This is program used to implement a simplified NAT. The program will read a list of NAT entries and list of flows, then output the flows after they have been processed.

(1) NAT file will have lot of entries, the format of entries looks like: 

**<ip>:<port>, <ip>:<port>**

There are three valid format of the the NAT entries, for example:

**1.1.1.1:8000,2.2.2.2:80**

***:80,3.3.3.3:90** (that's means if the port is 80, will be match, do not care about the IP  address)

**3.3.3.3:*,5.5.5.5:300** (that's means if the IP  address is 3.3.3.3, will be match, do not care about the port)

(2) The entries of Flow file will like 1.1.1.1:8000.

(3) The out file will indicate the match status, for example:

**1.1.1.1:8000->2.2.2.2:80** (matched)

**No nat match for 5.6.7.8:55555** (no matched)

## Implementation

Actually, for this problem, the main tasks is to find whether there is a rule in the NAT file that will match the flow entries. So, we should use a data structure that can find the target item quickly. The first solution is to use an array, and when we need to search a target, just traverse the array, and get the result, but, the time complexity is O(n), if there are many of flow entries, the performance will be pretty low.

The good solution is to use a Hashtable, because in Hashtable, find operation was a constant time O(1). We can define a <**key**,**value**> structure through Hashtable. In this problem, the key is the original IP and port pair of NAT entries, value will be the target IP and port pair of NAT entries. We just need to use every flow entries as a key to see if there is a value match the key. But, there is a problem, in NAT file, there are two different rules, we should deal with it separately. We can use another two Hashtable to store those rules, in first Hashtable, the key will be port, in second Hashtable, the key will be IP. 

In my program, the first step is to build the Hashmap by using NAT file, we read the NAT file by line, then use split function to split the every single entries, after split we can get the original IP and port pair and target IP and port pair, then we split the IP and port pair to get IP address and port, then insert those key and value to relative Hashmap, for example, if we get the original IP is '*', then we insert the port and target IP and port pair to the port Hashmap, we use the same method to deal with the other two rules.

Now we have a Hashmap, the second step is to read the flow file and to find whether there is a match item in the Hashmap. We split the flow entries to get the IP and port, then we use this as a key to search the relative Hashmap, if there is a matching entries, then write to the output file, otherwise, write no match NAT for given flow entries.

But, there is a tricky thing, let consider the special situation, for example, In NAT file, there are three rules:

**1.1.1.1:800,2.2.2.2:100**

**1.1.1.1:*,3.3.3.3:100**

***:800, 4.4.4.4:100**

And the give flow entry is **1.1.1.1:800**, so, which rules should be match, because we can see the give flow entry can match all of the rules typically. So, here, we define a priority level of those three rules, we first to choice match the rule 1, which have both IP and port pair, then we match rule 2, which just has IP address but not port, finally, we match the last one. 

## Test

#### Development Environment   

Language: C++11, 

OS: Ubuntu16.04, 

Compiler: g++ (Ubuntu 5.5.0-12ubuntu1~16.04)  5.5.0 20171010

#### How to run the program

./[target execute file]  {path of nat file} {path of  flow file} {output file}

#### How to test

I create a NAT file, flow file and add some relative entries to those file. Then run the program and check the result of output file whether is expect result.

#### Test case

**Test case 1:**

**NAT file:**

10.0.1.1:8080,192.168.0.1:80

10.0.1.1:8084,192.168.0.2:8080

10.0.1.1:8086,192.168.0.4:80

10.0.1.1:8082,192.168.0.3:80

10.0.1.2:8080,1.1.1.1:1

*:21,1.2.3.4:12

10.11.12.13:*,40.100.20.7:3389

**Flow file:**

10.0.1.1:8080

5.6.7.8:55555

10.0.1.1:8086

9.8.7.6:21

10.1.1.2:8080

34.65.12.9:22

10.0.1.2:8080

**Output file:**

10.0.1.1:8080->192.168.0.1:80

No nat match for 5.6.7.8:55555

10.0.1.1:8086->192.168.0.4:80

9.8.7.6:21->1.2.3.4:12

No nat match for 10.1.1.2:8080

No nat match for 34.65.12.9:22

10.0.1.2:8080->1.1.1.1:1

**Test case2:**

**NAT file:**

1.1.1.1:*,3.3.3.3:500

1.1.1.1:300,2.2.2.2:400

*:300,4.4.4.4:600

2.2.2.2:300,5.5.5.5:400

9.9.9.9:*,255.234.1.123:800

**Flow file:**

1.1.1.1:300

9.9.9.9:2000

10.10.10.10:400

**Output file:**

1.1.1.1:300->2.2.2.2:400

9.9.9.9:2000->255.234.1.123:800

No nat match for 10.10.10.10:400

In test file 2, there is three duplicate rules, according to priority level I defined, it's will first match the rule one, which has both IP and port.

## Optimize

In my program, I read the NAT file line by line to build the Hashmap, if the file is so large and there are many entries, maybe it will cost a lot of time. So, I was wondering, maybe we can use multi-thread to optimize the operation which read the NAT entries to build the Hasmap, but we should keep the thread safe, so, we should use thread safe data structure or leverage some mechanism to guarantee the thread safe. And if we use multi-thread, we also need to use a type of singleton which is thread safe.

We also can use multi-thread to search the flow entry in the Hashmap. But also need to consider thread safe problem.